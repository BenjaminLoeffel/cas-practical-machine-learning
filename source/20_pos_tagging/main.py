import json
import logging
from pathlib import Path
from typing import Dict, Optional

from loggin_builder import LoggingBuilder

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()


class Word:
    _word_str: str
    _pos_tags: Dict[str, int]

    def __init__(self, word_str: str, pos_tags: Optional[Dict[str, int]] = None):
        self._word_str = word_str
        if pos_tags is None:
            pos_tags = {}
        self._pos_tags = pos_tags

    def add_pos_tag(self, pos_tag: str):
        count = self._pos_tags.get(pos_tag)
        if count is not None:
            self._pos_tags[pos_tag] += 1
        else:
            self._pos_tags[pos_tag] = 1

    def get_count_by_pos_tag(self, pos_tag: str) -> Optional[int]:
        return self._pos_tags.get(pos_tag)

    @property
    def word_str(self) -> str:
        return self._word_str

    @property
    def pos_tags(self) -> Dict[str, int]:
        return self._pos_tags


class PosDatabase:
    _words: Dict[str, Word]

    def __init__(self, words: Optional[Dict[str, Word]] = None):
        if words is None:
            words = {}
        self._words = words

    def add_word(self, word: Word):
        if self._words.get(word.word_str) is not None:
            raise KeyError("word already present, exception raised to avoid overwrite")
        self._words[word.word_str] = word

    def get_word(self, word_str: str) -> Optional[Word]:
        return self._words.get(word_str)

    @property
    def words(self) -> Dict[str, Word]:
        return self._words


class PosDatabaseRepository:

    def __init__(self):
        pass

    def _word_to_dict(self, word: Word) -> Dict[str, int]:
        pos_tags = {}
        for pos_tag, count in word.pos_tags.items():
            pos_tags[pos_tag] = count
        return pos_tags

    def _dict_to_word(self, word_str: str, pos_tags: Dict[str, int]) -> Word:
        return Word(word_str=word_str, pos_tags=pos_tags)

    def save(self, pos_database: PosDatabase, filename: Path):
        database_dict: Dict[str, Dict[str, int]] = {}
        for word_str, word in pos_database.words.items():
            database_dict[word_str] = self._word_to_dict(word=word)
        with open(filename, "w") as f:
            json.dump(obj=database_dict, fp=f)
            _logger.info(f"saved pos database to {filename}")

    def load(self, filename: Path) -> PosDatabase:
        with open(filename, "r") as f:
            content = json.load(f)
        words: Dict[str, Word] = {}
        for word_str, post_tags in content.items():
            words[word_str] = self._dict_to_word(word_str, pos_tags=post_tags)
        return PosDatabase(words=words)


if __name__ == "__main__":

    try:
        _logger.info("job started")

        # Let this section run to generate database
        if True == False:

            import nltk

            nltk.download('punkt')
            nltk.download('gutenberg')
            nltk.download('averaged_perceptron_tagger')
            from nltk.corpus import gutenberg

            fileid = 'melville-moby_dick.txt'
            text = gutenberg.raw(fileid)
            _logger.info(f"len(text)={len(text)}")

            sentences = nltk.sent_tokenize(text)
            _logger.info(f"len(sentences)={len(sentences)}")

            pos_database = PosDatabase()

            for s in sentences:
                tokens = nltk.word_tokenize(s)
                word_pos_list = nltk.pos_tag(tokens)

                for word_pos in word_pos_list:
                    word_str = word_pos[0].lower()
                    pos_tag = word_pos[1]
                    word = pos_database.get_word(word_str=word_str)
                    if word is None:
                        word = Word(word_str=word_str)
                        word.add_pos_tag(pos_tag=pos_tag)
                        pos_database.add_word(word)
                    else:
                        word.add_pos_tag(pos_tag=pos_tag)

            pos_database_repository = PosDatabaseRepository()
            pos_database_repository.save(pos_database=pos_database,
                                         filename=Path("PosDatabase.json"))

        pos_database_repository = PosDatabaseRepository()
        pos_database = pos_database_repository.load(Path("PosDatabase.json"))
        a = 1

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
