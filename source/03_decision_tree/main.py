import logging

from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier

from loggin_builder import LoggingBuilder
from visualization import plot_histogram, plot_scatter, plot_decision_tree, plot_decision_tree_modern

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()

if __name__ == "__main__":

    try:
        _logger.info("job started")
        iris = datasets.load_iris()

        figure = plot_histogram(iris)
        figure.savefig("histogram.png")

        figure = plot_scatter(iris)
        figure.savefig("scatter.png")

        classifier = DecisionTreeClassifier()
        classifier.fit(iris.data[:-1], iris.target[:-1])  # use all but the last sample for training

        figure = plot_decision_tree(classifier, iris)
        figure.savefig("decision_tree.png")

        viz = plot_decision_tree_modern(classifier, iris)
        viz.save("decision_tree.svg")

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
