import sklearn
from dtreeviz.trees import dtreeviz
from matplotlib import pyplot as plt
from sklearn import tree
from sklearn.base import ClassifierMixin


def plot_histogram(iris: sklearn.utils.Bunch) -> plt.Figure:
    figure = plt.figure()
    plt.hist(iris.target, bins=(0, 1, 2, 3), edgecolor="black")
    return figure


def plot_scatter(iris: sklearn.utils.Bunch) -> plt.Figure:
    X = iris.data[:, :2]  # we only take the first two features.
    y = iris.target
    figure = plt.figure(2, figsize=(8, 6))
    plt.clf()
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1, edgecolor="k")
    plt.xlabel("Sepal length")
    plt.ylabel("Sepal width")
    plt.xticks(())
    plt.yticks(())
    return figure


def plot_decision_tree(classifier:ClassifierMixin, iris: sklearn.utils.Bunch) -> plt.Figure:
    tree.plot_tree(classifier, feature_names=iris.feature_names, class_names=iris.target_names, filled=True,
                   rounded=True)
    figure = plt.gcf()
    return figure


def plot_decision_tree_modern(classifier:ClassifierMixin, iris: sklearn.utils.Bunch):
    viz = dtreeviz(classifier, iris.data, iris.target,
                   target_name="target",
                   feature_names=iris.feature_names,
                   class_names=list(iris.target_names))
    return viz
