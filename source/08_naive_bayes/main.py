import logging
from pathlib import Path

import pandas as pd
from sklearn import datasets
from sklearn.cluster import KMeans

from evaluation import ClusteringEvaluation
from loggin_builder import LoggingBuilder

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()

if __name__ == "__main__":

    try:
        _logger.info("job started")
        set_plotly_defaults()

        iris = datasets.load_iris()

        features = iris.data
        labels = iris.target

        clustering = KMeans(n_clusters=3)
        predicted_labels = clustering.fit_predict(features)

        print(ClusteringEvaluation(features=features, true_labels=labels, predicted_labels=predicted_labels))

        visualization_dataframe = pd.DataFrame(data=features, columns=iris.feature_names)
        visualization_dataframe["species"] = labels
        visualization_dataframe["predicted_label"] = predicted_labels

        figure = plot_kmeans_3d(dataframe=visualization_dataframe,
                                x_column="sepal length (cm)",
                                y_column="sepal width (cm)",
                                z_column="petal width (cm)",
                                color_column="species",
                                marker_column="predicted_label")
        PlotlyFigure(figure=figure, filename=Path("kmeans.html")).figure_to_html_file(auto_open=True)

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
