import logging
from pathlib import Path

import pandas as pd
import dtale

from loggin_builder import LoggingBuilder

from pandas_profiling import ProfileReport

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()

if __name__ == "__main__":

    try:
        _logger.info("job started")

        data_path = Path.cwd().joinpath("data")

        data_file = data_path.joinpath("bank.csv")

        dataframe = pd.read_csv(data_file, sep=";")

        profile = ProfileReport(dataframe, title="Pandas Profiling Report")

        profile.to_file("report.html")

        dtale.show(dataframe, subprocess=False)

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
