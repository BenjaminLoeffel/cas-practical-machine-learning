import logging
from pathlib import Path

from app.dataset_service import DatasetService
from loggin_builder import LoggingBuilder

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()

if __name__ == "__main__":

    try:
        _logger.info("job started")

        dataset_service = DatasetService()
        dataset = dataset_service.from_directory(datapath=Path.cwd().joinpath("data"))
        a = 1



    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
