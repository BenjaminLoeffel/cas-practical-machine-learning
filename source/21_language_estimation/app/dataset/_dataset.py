from typing import List

from app.tweet import Tweet


class Dataset:
    _tweets: List[Tweet]

    def __init__(self, tweets: List[Tweet]):
        self._tweets = tweets

    @property
    def tweets(self) -> List[Tweet]:
        return self._tweets
