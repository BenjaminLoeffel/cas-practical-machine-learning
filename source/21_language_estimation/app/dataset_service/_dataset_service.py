from pathlib import Path
from typing import List

from app.dataset import Dataset
from app.language import Language
from app.tweet import Tweet


class DatasetService:

    def __init__(self):
        pass

    def from_directory(self, datapath: Path) -> Dataset:
        tweets: List[Tweet] = []
        for language in Language:
            language_path = datapath.joinpath(language.value)
            tweet_files = language_path.glob("*.txt")
            for file in tweet_files:
                with open(file, "r", encoding="utf-8") as f:
                    content = f.read()
                    tweets.append(Tweet(language=language, content=content))
        return Dataset(tweets=tweets)
