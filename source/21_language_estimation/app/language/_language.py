from enum import Enum


class Language(str, Enum):
    GERMAN = "de_DE"
    ENGLISH = "en_UK"
    SPANISH = "es_ES"
    FRENCH = "fr_FR"
    ITALIAN = "it_IT"
    DUTCH = "nl_NL"
