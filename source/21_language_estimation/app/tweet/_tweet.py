from app.language import Language


class Tweet:
    _language: Language
    _content: str

    def __init__(self, language: Language, content: str):
        self._language = language
        self._content = content

    @property
    def language(self) -> Language:
        return self._language

    @property
    def content(self) -> str:
        return self._content
