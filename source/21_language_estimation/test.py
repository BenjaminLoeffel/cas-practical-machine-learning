from pathlib import Path
from typing import List

import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import MultinomialNB


def read_file(file: Path) -> str:
    with open(file, "r", encoding="uft-8") as f:
        text = f.read()
    return text


def get_charset() -> List[str]:
    charset = set()
    for lanugage_dir in lanugage_dirs:
        data_dir = Path.cwd().joinpath("data")
        data_files = data_dir.joinpath(lanugage_dir).glob("*.txt")
        for data_file in data_files:
            text = read_file(data_file)
            for c in text:
                charset.update(c)
    return sorted(charset)


# bigrams as features

# create empty dictionary of character bigrams
def get_bigram_dict(charset):
    bigram_dict = {}
    for c1 in charset:
        for c2 in charset:
            bigram_dict[c1 + c2] = 0
    return bigram_dict


# create bigram dictionary from text, iteratively if wanted
def add_to_bigram_dict(bigram_dict, text):
    # count bigrams for given text
    i = 0
    while i < len(text) - 1:
        bigram = text[i].lower() + text[i + 1].lower()
        if bigram in bigram_dict:
            old_count = bigram_dict[bigram]
            bigram_dict[bigram] = old_count + 1
        i += 1

    return bigram_dict


# convert to an array of bigram counts
def bigram_dict_to_feature_array(bigram_dict):
    bigram_array = []
    for b, bc in bigram_dict.items():
        bigram_array.append(bc)
    return bigram_array


bigram_dict = get_bigram_dict(get_charset())
print(bigram_dict_to_feature_array(add_to_bigram_dict(bigram_dict, "Ich bin ein Text.")))

# test and training data


lanugage_dirs = ["de_DE", "en_UK", "es_ES", "fr_FR", "it_IT", "nl_NL"]

samples = 0
for lanugage_dir in lanugage_dirs:
    data_dir = Path.cwd().joinpath("data")
    data_files = data_dir.joinpath(lanugage_dir).glob("*.txt")
    samples += len(list(data_files))

charset = get_charset()
features = len(get_bigram_dict(charset))
print("features: ", features)

X = np.empty([samples, features], dtype=int)
y = []

sample = 0
for lanugage_dir in lanugage_dirs:
    data_dir = Path.cwd().joinpath("data")
    data_files = data_dir.joinpath(lanugage_dir).glob("*.txt")
    for f in data_files:
        y.append(lanugage_dirs)
        bigram_dict = get_bigram_dict(charset)
        add_to_bigram_dict(bigram_dict, read_file(lanugage_dirs + '/' + f))
        X[sample] = np.array(bigram_dict_to_feature_array(bigram_dict))
        sample += 1

nb = MultinomialNB()
scores = cross_val_score(nb, X, y, cv=10)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std()))
