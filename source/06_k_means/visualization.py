import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


def set_plotly_defaults():
    import plotly.io as pio
    pio.templates.default = "plotly_white"


def plot_kmeans_3d(dataframe: pd.DataFrame, x_column: str, y_column: str, z_column: str, color_column: str,
                   marker_column: str) -> go.Figure:
    figure = px.scatter_3d(dataframe, x=x_column, y=y_column, z=z_column, color=color_column, symbol=marker_column)
    figure.update_layout(coloraxis_colorbar={'yanchor': "top", 'y': 1, 'x': 0, 'ticks': "outside"})
    return figure
