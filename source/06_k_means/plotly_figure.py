import io
import logging
from pathlib import Path
from typing import Optional

import plotly.graph_objects as go

_logger = logging.getLogger(__file__)


class PlotlyFigure:
    _figure: go.Figure
    _filename: Path

    def __init__(self, figure: go.Figure, filename: Path):
        self._figure = figure
        self._filename = filename

    @property
    def figure(self) -> go.Figure:
        return self._figure

    @property
    def filename(self) -> Path:
        return self._filename

    def _get_config(self) -> dict:
        return {"displayModeBar": True,
                "displaylogo": False,
                "toImageButtonOptions": {"format": "png",
                                         "filename": self._filename.stem,
                                         "width": None,
                                         "height": None},
                "modeBarButtonsToRemove": ["resetCameraDefault3d"]}

    def figure_to_string_io(self, full_html: Optional[bool] = True) -> io.StringIO:
        buffer = io.StringIO()
        self._figure.write_html(buffer, config=self._get_config(), full_html=full_html)
        buffer.seek(0)
        return buffer

    def figure_to_html_file(self, auto_open: bool = False):
        self._filename.parent.mkdir(parents=True, exist_ok=True)
        self._figure.write_html(file=self._filename,
                                auto_open=auto_open,
                                config=self._get_config())
        _logger.info(f"plot exported to: {self._filename}")

# import io
# import logging
# from pathlib import Path
# from pathlib import WindowsPath
# from typing import Union
#
# from plotly.graph_objs import Figure
#
# _logger = logging.getLogger(__file__)
#
#
# def _get_config(filename: Path) -> dict:
#     return {"displayModeBar": True,
#             "displaylogo": False,
#             "toImageButtonOptions": {"format": "png",
#                                      "filename": filename.stem,
#                                      "width": None,
#                                      "height": None},
#             "modeBarButtonsToRemove": ["resetCameraDefault3d"]}
#
#
# def figure_to_html_file(figure: Figure, filename: Union[str, Path], auto_open: bool = False):
#     if type(filename) == str:
#         export_filename = filename.replace(".html", "")
#     elif type(filename) == WindowsPath:
#         filename.parent.mkdir(parents=True, exist_ok=True)
#         export_filename = filename.stem
#         filename = filename.as_posix()
#     else:
#         raise ValueError("str or pathlib.Path like object is _expected")
#     figure.write_html(file=filename,
#                       auto_open=auto_open,
#                       config=_get_config(export_filename))
#     _logger.info(f"plot exported to: {filename}")
#
#
# def figure_to_html_string(figure: Figure, filename: Path) -> str:
#     html = figure.to_html(config=_get_config(filename))
#     return html
#
#
# def figure_to_string_io(figure: Figure, filename: Path) -> io.StringIO:
#     buffer = io.StringIO()
#     figure.write_html(buffer, config=_get_config(filename=filename))
#     buffer.seek(0)
#     return buffer
#
#
# def figure_to_bytes(figure: Figure, filename: Path) -> bytes:
#     buffer = io.StringIO(figure_to_html_string(figure, filename))
#     buffer.seek(0)
#     return buffer.getvalue()
