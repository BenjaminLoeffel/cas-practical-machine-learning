import numpy as np
from sklearn import metrics


class ClusteringEvaluation:
    features: np.ndarray
    true_labels: np.ndarray
    predicted_labels: np.ndarray

    def __init__(self, features: np.ndarray, true_labels: np.ndarray, predicted_labels: np.ndarray):
        self.features = features
        self.true_labels = true_labels
        self.predicted_labels = predicted_labels

    @property
    def homogeneity_score(self):
        return metrics.homogeneity_score(self.true_labels, self.predicted_labels)

    @property
    def completeness_score(self):
        return metrics.completeness_score(self.true_labels, self.predicted_labels)

    @property
    def adjusted_rand_score(self):
        return metrics.adjusted_rand_score(self.true_labels, self.predicted_labels)

    @property
    def silhouette_score(self):
        return metrics.silhouette_score(self.features, self.predicted_labels)

    def __str__(self):
        return f"homogeneity_score: {self.homogeneity_score}\n" \
               f"completeness_score: {self.completeness_score}\n" \
               f"adjusted_rand_score: {self.adjusted_rand_score}\n" \
               f"silhouette_score: {self.silhouette_score}"
