import logging
from pathlib import Path
from typing import List
from typing import Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from app.dataset_service import DatasetRepository
from loggin_builder import LoggingBuilder

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()


def set_matplotlib_defaults(figsize: Optional[Tuple[float, float]] = None, agg: bool = False):
    if figsize is None:
        figsize = (18, 9)
    import matplotlib as mpl

    mpl.rcParams["figure.figsize"] = figsize
    mpl.rcParams["figure.subplot.left"] = 0.06
    mpl.rcParams["figure.subplot.bottom"] = 0.07
    mpl.rcParams["figure.subplot.right"] = 0.94
    mpl.rcParams["figure.titlesize"] = 22
    mpl.rcParams["figure.titleweight"] = "normal"
    mpl.rcParams["axes.titlesize"] = 18
    mpl.rcParams["axes.titlepad"] = 10
    mpl.rcParams["axes.labelsize"] = 14
    mpl.rcParams["savefig.format"] = "pdf"

    if agg:
        mpl.use("Agg")


class HeatmapVisualizer:

    def __init__(self):
        pass

    def plot(self, ax: plt.Axes, values: np.ndarray, column_names: List[str]) -> plt.Axes:
        ax = sns.heatmap(ax=ax,
                         data=values,
                         annot=True,
                         fmt=".3f",
                         cmap="viridis",
                         xticklabels=column_names,
                         yticklabels=column_names)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
        ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
        return ax


if __name__ == "__main__":

    try:
        _logger.info("job started")

        set_matplotlib_defaults()

        articles = DatasetRepository.load(filename=Path("dataset.json"))

        similarities = np.load("similarities.npy")

        heatmap_visualizer = HeatmapVisualizer()

        figure = plt.figure()
        ax = figure.add_subplot(1, 1, 1)
        heatmap_visualizer.plot(ax=ax, values=similarities,
                                column_names=[f"{article.title} / {article.category}" for article in articles])
        figure.tight_layout()
        figure.savefig("similarities.png")

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
