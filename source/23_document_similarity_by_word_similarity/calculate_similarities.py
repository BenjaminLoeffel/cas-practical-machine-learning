import logging
from pathlib import Path
from typing import List

import nltk
import numpy as np

from app.dataset_service import DatasetRepository
from app.wikipedia_article import WikipediaArticle
from loggin_builder import LoggingBuilder

nltk.download("stopwords")
nltk.download("wordnet")
nltk.download("omw-1.4")
_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()


class ProcessedWikipediaArticle:
    _article: WikipediaArticle
    _wordlist: List[str]

    def __init__(self, article: WikipediaArticle, wordlist: List[str]):
        self._article = article
        self._wordlist = wordlist

    @property
    def article(self) -> WikipediaArticle:
        return self._article

    @property
    def wordlist(self) -> List[str]:
        return self._wordlist


class Preprocessor:

    def __init__(self, article: WikipediaArticle, top_n_words: int = 0):
        self._article = article
        self._top_n_words = top_n_words

    def _tokenize(self) -> List[str]:
        wordlist = nltk.word_tokenize(self._article.content)
        return wordlist

    def _remove_punctuation(self, wordlist: List[str]) -> List[str]:
        punctuation = {'.', ',', ';', ':', '(', ')', '[', ']', '{', '}', '\"', '\'', '\'\'', '\`', '\`\`', '==', '===',
                       '====', '-'}
        wordlist_stripped = [w for w in wordlist if w not in punctuation]
        wordlist_stripped = [w for w in wordlist_stripped if len(w) > 2]
        return wordlist_stripped

    def _remove_stopwords(self, wordlist: List[str]) -> List[str]:
        stopwords = nltk.corpus.stopwords.words('english')
        wordlist_stripped = [w for w in wordlist if w.lower() not in stopwords]
        return wordlist_stripped

    def _lowercase(self, wordlist: List[str]) -> List[str]:
        return [w.lower() for w in wordlist]

    def _lemmatize(self, wordlist: List[str]) -> List[str]:
        wnl = nltk.WordNetLemmatizer()
        wordlist_lemmatized = [wnl.lemmatize(w) for w in wordlist]
        return wordlist_lemmatized

    def _frequent_words(self, wordlist: List[str], topn: int) -> List[str]:
        fd = nltk.FreqDist(wordlist)
        fw = [fwt[0] for fwt in fd.most_common(topn)]
        return fw

    def preprocess(self) -> ProcessedWikipediaArticle:
        wordlist = self._tokenize()
        wordlist = self._lowercase(wordlist)
        wordlist = self._remove_stopwords(wordlist)
        wordlist = self._remove_punctuation(wordlist)
        wordlist = self._lemmatize(wordlist)
        if (self._top_n_words > 0):
            wordlist = self._frequent_words(wordlist, self._top_n_words)
        return ProcessedWikipediaArticle(article=self._article, wordlist=wordlist)


class WordSimilarityCalculator:

    @staticmethod
    def get_similarity(word_0: str, word_1: str) -> float:
        # handle trivial case
        if word_0 == word_1:
            return 1

        # handle case where no syns exist at all
        syns_0 = nltk.corpus.wordnet.synsets(word_0)
        if len(syns_0) == 0:
            return 0

        syns_1 = nltk.corpus.wordnet.synsets(word_1)
        if len(syns_1) == 0:
            return 0

        # full factorial combination of all syns
        # preparation for running average calculation
        sum = 0
        count = 0
        for syn_0 in syns_0:
            for syn_1 in syns_1:
                similarity = syn_0.path_similarity(syn_1)
                if similarity is not None:
                    sum += similarity
                    count += 1
        return sum / count


class ArticleSimilarityCalculator:

    def __init__(self):
        pass

    def get_similarity(self, wordlist_0: List[str], wordlist_1: List[str]) -> float:
        # full factorial combination of all words
        # preparation for running average calculation
        sum = 0
        count = 0
        for word_0 in wordlist_0:
            for word_1 in wordlist_1:
                similarity = WordSimilarityCalculator.get_similarity(word_0, word_1)
                sum += similarity
                count += 1
        return sum / count


if __name__ == "__main__":

    try:
        _logger.info("job started")

        articles = DatasetRepository.load(filename=Path("dataset.json"))

        n_articles = len(articles)

        preprocesses_articles = [Preprocessor(article=article, top_n_words=30).preprocess() for article in articles]

        similarities = np.zeros((n_articles, n_articles))

        for index_0, preprocesses_article_0 in enumerate(preprocesses_articles):
            for index_1, preprocesses_article_1 in enumerate(preprocesses_articles):
                article_similarity_calculator = ArticleSimilarityCalculator()
                similarity = article_similarity_calculator.get_similarity(wordlist_0=preprocesses_article_0.wordlist,
                                                                          wordlist_1=preprocesses_article_1.wordlist)
                _logger.info(
                    f"similarity between {preprocesses_article_0.article.title} and {preprocesses_article_1.article.title} = {similarity}")
                similarities[index_0, index_1] = similarity

        np.save("similarities", similarities)

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
