from app.wikipedia_article_category import WikipediaArticleCategory


class WikipediaArticle:
    _title: str
    _content: str
    _category: WikipediaArticleCategory

    def __init__(self, title: str, content: str, category: WikipediaArticleCategory):
        self._title = title
        self._content = content
        self._category = category

    @property
    def title(self) -> str:
        return self._title

    @property
    def content(self) -> str:
        return self._content

    @property
    def category(self) -> WikipediaArticleCategory:
        return self._category
