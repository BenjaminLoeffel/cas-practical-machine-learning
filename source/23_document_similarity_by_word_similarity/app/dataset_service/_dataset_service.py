import json
from pathlib import Path
from typing import List

from app.wikipedia_article import WikipediaArticle
from app.wikipedia_article_category import WikipediaArticleCategory
from app.wikipedia_service import WikipediaService


class DatasetService:

    def __init__(self, wikipedia_service: WikipediaService):
        self._wikipedia_service = wikipedia_service
        self._articles: List[WikipediaArticle] = []

    def get_wikipedia_article(self, title: str, category: WikipediaArticleCategory):
        content = self._wikipedia_service.get_wikipedia_article(title)
        self._articles.append(WikipediaArticle(title=title, content=content, category=category))

    @property
    def articles(self) -> List[WikipediaArticle]:
        return self._articles


class DatasetRepository:

    @staticmethod
    def save(articles: List[WikipediaArticle], filename: Path):
        documents: List[dict] = []
        for article in articles:
            document = {"title": article.title,
                        "category": article.category,
                        "content": article.content}
            documents.append(document)
        with open(filename, "w") as f:
            json.dump(obj=documents, fp=f)

    @staticmethod
    def load(filename: Path) -> List[WikipediaArticle]:
        with open(filename, "r") as f:
            content = json.load(f)
        articles: List[WikipediaArticle] = []
        for article in content:
            articles.append(WikipediaArticle(title=article["title"],
                                             category=WikipediaArticleCategory(article["category"]),
                                             content=article["content"]))
        return articles
