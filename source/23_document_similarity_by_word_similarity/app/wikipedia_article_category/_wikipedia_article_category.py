from enum import Enum


class WikipediaArticleCategory(str, Enum):
    NLP = "Natural Language Processing"
    ML = "Machine Learning"
    NUTRITION = "Nutrition"
    CALCULUS = "Calculus"
