import logging
from pathlib import Path

from app.dataset_service import DatasetService, DatasetRepository
from app.wikipedia_article_category import WikipediaArticleCategory
from app.wikipedia_service import WikipediaService
from loggin_builder import LoggingBuilder

_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()

if __name__ == "__main__":

    try:
        _logger.info("job started")

        wikipedia_service = WikipediaService()

        dataset_service = DatasetService(wikipedia_service=wikipedia_service)

        # NLP
        dataset_service.get_wikipedia_article(title="Machine translation", category=WikipediaArticleCategory.NLP)
        dataset_service.get_wikipedia_article(title="Semantic analytics", category=WikipediaArticleCategory.NLP)
        dataset_service.get_wikipedia_article(title="Bag-of-words model", category=WikipediaArticleCategory.NLP)
        dataset_service.get_wikipedia_article(title="Question answering", category=WikipediaArticleCategory.NLP)

        # ML
        dataset_service.get_wikipedia_article(title="Generative model", category=WikipediaArticleCategory.ML)
        dataset_service.get_wikipedia_article(title="Adversarial machine learning",
                                              category=WikipediaArticleCategory.ML)
        dataset_service.get_wikipedia_article(title="Early stopping", category=WikipediaArticleCategory.ML)
        dataset_service.get_wikipedia_article(title="Anomaly detection", category=WikipediaArticleCategory.ML)

        # Nutrition
        dataset_service.get_wikipedia_article(title="Alliin", category=WikipediaArticleCategory.NUTRITION)
        dataset_service.get_wikipedia_article(title="Glycogen", category=WikipediaArticleCategory.NUTRITION)
        dataset_service.get_wikipedia_article(title="Gluten", category=WikipediaArticleCategory.NUTRITION)
        dataset_service.get_wikipedia_article(title="Fructose", category=WikipediaArticleCategory.NUTRITION)

        # Calculus
        dataset_service.get_wikipedia_article(title="Calculus", category=WikipediaArticleCategory.CALCULUS)
        dataset_service.get_wikipedia_article(title="Infinitesimal", category=WikipediaArticleCategory.CALCULUS)
        dataset_service.get_wikipedia_article(title="Euler spiral", category=WikipediaArticleCategory.CALCULUS)
        dataset_service.get_wikipedia_article(title="Differential calculus", category=WikipediaArticleCategory.CALCULUS)

        articles = dataset_service.articles

        DatasetRepository.save(articles=articles, filename=Path("dataset.json"))

    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
