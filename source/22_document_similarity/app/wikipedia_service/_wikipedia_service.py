import wikipedia

wikipedia.set_lang("en")


class WikipediaService:

    def __init__(self):
        pass

    def get_wikipedia_article(self, title: str) -> str:
        content = wikipedia.page(title).content
        return content
