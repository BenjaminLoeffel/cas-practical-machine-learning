import logging
from pathlib import Path
from typing import List

import nltk
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer

from app.dataset_service import DatasetRepository
from app.wikipedia_article import WikipediaArticle
from loggin_builder import LoggingBuilder

nltk.download("stopwords")
_logger = logging.getLogger()
logging_builder = LoggingBuilder(_logger)
logging_builder.add_console_handler()


def remove_punctuation(wordlist: List[str]) -> List[str]:
    punctuation = {'.', ',', ';', ':', '(', ')', '[', ']', '{', '}', '\"', '\'', '\'\'', '\`', '\`\`', '==', '===',
                   '====', '-'}
    wordlist_stripped = [w for w in wordlist if w not in punctuation]
    wordlist_stripped = [w for w in wordlist_stripped if len(w) > 2]
    return wordlist_stripped


def remove_stopwords(wordlist: List[str]) -> List[str]:
    stopwords = nltk.corpus.stopwords.words('english')
    wordlist_stripped = [w for w in wordlist if w.lower() not in stopwords]
    return wordlist_stripped


def lowercase(wordlist: List[str]) -> List[str]:
    return [w.lower() for w in wordlist]


def get_wordlist_from_wikipedia_article(article: WikipediaArticle) -> List[str]:
    wordlist = nltk.word_tokenize(article.content)
    wordlist = lowercase(wordlist)
    wordlist = remove_stopwords(wordlist)
    wordlist = remove_punctuation(wordlist)
    return wordlist


if __name__ == "__main__":

    try:
        _logger.info("job started")

        articles = DatasetRepository.load(filename=Path("dataset.json"))

        pages: List[str] = []

        for article in articles:
            word_list = get_wordlist_from_wikipedia_article(article)
            pages.append(" ".join(word_list))

        vectorizer = TfidfVectorizer()
        X = vectorizer.fit_transform(pages)
        feature_names_out = vectorizer.get_feature_names_out()

        true_k = 4
        model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=10)
        model.fit(X)

        for i in range(len(articles)):
            _logger.info(f"{articles[i].title}, {articles[i].category}, {model.labels_[i]}")


    except:
        _logger.exception("exception occurred", exc_info=True)

    finally:
        _logger.info("job finished")
